// Package v1 contains the full set of handler functions and routes
// supported by the v1 web api.
package v1

import (
	"log/slog"
	"net/http"

	"code.europa.eu/healthdataeu-nodes/eu-connector/api/services/data-permit/handlers/v1/budibasegrp"
	"code.europa.eu/healthdataeu-nodes/eu-connector/api/services/data-permit/handlers/v1/permitgrp"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/edelivery/domibus"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
)

// Config contains all the mandatory systems required by handlers.
type Config struct {
	Logger      *slog.Logger
	DomibusSOAP *domibus.SOAP
	Party       domibus.Party
	CatalogIDs  map[uuid.UUID]string
}

// Routes binds all the version 1 routes.
func Routes(router *mux.Router, cfg Config) {
	pgh := permitgrp.New(cfg.Logger, cfg.DomibusSOAP, cfg.Party, cfg.CatalogIDs)
	router.HandleFunc("/create/application", pgh.Create).Methods(http.MethodPost)

	// Message dispatcher routes.
	bgh := budibasegrp.New(cfg.Logger, cfg.DomibusSOAP, cfg.Party, cfg.CatalogIDs)
	router.HandleFunc("/update/application", bgh.UpdateApplication).Methods(http.MethodPost)
}
