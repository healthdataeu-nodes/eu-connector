// Package permitgrp maintains the group of handlers for permit access.
package permitgrp

import (
	"encoding/xml"
	"fmt"
	"io"
	"log/slog"
	"net/http"

	"code.europa.eu/healthdataeu-nodes/eu-connector/api/services/data-permit/core/permit"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/edelivery/domibus"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/web"

	"github.com/google/uuid"
)

const (
	createApplication = "create/application"
)

// Handler manages the set of budibase endpoints.
type Handler struct {
	logger      *slog.Logger
	domibusSOAP *domibus.SOAP
	party       domibus.Party
	catalogIDs  map[uuid.UUID]string
}

// New constructs a handlers for route access.
func New(log *slog.Logger, domibusSOAP *domibus.SOAP, party domibus.Party, catalogIDs map[uuid.UUID]string) *Handler {
	return &Handler{
		logger:      log,
		domibusSOAP: domibusSOAP,
		party:       party,
		catalogIDs:  catalogIDs,
	}
}

// Create creates a new permit message.
func (h *Handler) Create(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	body, err := io.ReadAll(r.Body)
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		return
	}

	perm, err := permit.New(body)
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		return
	}

	h.logger.Info("create-permit",
		"permit", perm)

	b64Payload, err := permit.CreateContent(createApplication, perm)
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		return
	}

	// Set the recipient.
	recipient, ok := h.catalogIDs[perm.Application.CatalogID]
	if !ok {
		web.Respond(r.Context(), w,
			web.NewErrorResponse(fmt.Sprintf("no node for this catalog ID: %q", perm.Application.CatalogID)),
			http.StatusBadRequest)
		return
	}

	h.party.Recipient = recipient

	w.Header().Set("Content-Type", "application/soap+xml")
	domiResponse, err := h.domibusSOAP.SubmitMsgRequest(r.Context(), b64Payload, "", h.party)
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusAccepted)

	if err = xml.NewEncoder(w).Encode(domiResponse); err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		return
	}
}
