// Package budibasegrp contains handlers proper to budibase, we may don't need
// it in the future.
package budibasegrp

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"net/url"
	"time"

	"code.europa.eu/healthdataeu-nodes/eu-connector/api/services/data-permit/core/permit"
	"code.europa.eu/healthdataeu-nodes/eu-connector/foundation/trackmsg"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/edelivery/domibus"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/externalapi"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/web"

	"github.com/google/uuid"
)

const (
	baseURL = "https://budibase.app/api/public/v1"
	appID   = "app_lionel_a1272fae6a544602b10ffc81c81fe68a"
	apiKey  = "516e20859e7ea6a77ee56ba377792b89-986e8fbf69114756872fbef677bb407d97feccf4a5bb191479ab8833f1ecd135e29f6736fdb911"
	tableID = "ta_3a3165d965ec4aad8c8ab20b1badcae6"
)

type Handler struct {
	logger      *slog.Logger
	domibusSOAP *domibus.SOAP
	party       domibus.Party
	httpCLient  *http.Client
	catalogIDs  map[uuid.UUID]string
}

func New(logger *slog.Logger, domibusSOAP *domibus.SOAP, party domibus.Party, catalogIDs map[uuid.UUID]string) *Handler {
	return &Handler{
		logger:      logger,
		domibusSOAP: domibusSOAP,
		party:       party,
		httpCLient:  externalapi.NewHTTPClient(logger),
		catalogIDs:  catalogIDs,
	}
}

// =======================================================================================

// Equal is need to get a row.
type Equal struct {
	ID1 int
}

// Query is needed to get a row.
type Query struct {
	Equal Equal `json:"equal"`
}

// Request is the body request to get a row.
type Request struct {
	Query Query `json:"query"`
}

// =======================================================================================

// Data is needed for getting data response.
type Data struct {
	ID string `json:"_id"`
}

// Response is the response for getting a row.
type Response struct {
	Data []Data `json:"data"`
}

// searchRow will search the row where is located the permit application needed
// in budibase.
func (h *Handler) searchRow(ctx context.Context, applicationID int) (string, error) {
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	bReq := Request{
		Query: Query{
			Equal: Equal{
				ID1: applicationID,
			},
		},
	}

	jsonBodyReq, err := json.Marshal(bReq)
	if err != nil {
		return "", fmt.Errorf("marshaling request body: %w", err)
	}

	u, err := url.JoinPath(baseURL, "tables", tableID, "rows", "search")
	if err != nil {
		return "", fmt.Errorf("create budibase url: %w", err)
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, u, bytes.NewBuffer(jsonBodyReq))
	if err != nil {
		return "", fmt.Errorf("creating a new request: %w", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("x-budibase-app-id", appID)
	req.Header.Set("x-budibase-api-key", apiKey)

	resp, err := h.httpCLient.Do(req)
	if err != nil {
		return "", fmt.Errorf("doing request: %w", err)
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("read response body: %w", err)
	}

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("status code: %d %s", resp.StatusCode, string(body))
	}

	var respBody Response
	if err := json.Unmarshal(body, &respBody); err != nil {
		return "", fmt.Errorf("unmarshal response body: %w", err)
	}

	if len(respBody.Data) == 0 {
		return "", fmt.Errorf("no data found: %+v", respBody)
	}

	rowID := respBody.Data[0].ID

	return rowID, nil
}

type UpdateRequest struct {
	Status string
}

// updateRow updates the row in budibase.
func (h *Handler) updateRow(ctx context.Context, rowID string, status string) error {
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	uReq := UpdateRequest{
		Status: status,
	}

	jsonBodyReq, err := json.Marshal(uReq)
	if err != nil {
		return fmt.Errorf("marshaling request body: %w", err)
	}

	u, err := url.JoinPath(baseURL, "tables", tableID, "rows", rowID)
	if err != nil {
		return fmt.Errorf("create budibase url: %w", err)
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodPut, u, bytes.NewBuffer(jsonBodyReq))
	if err != nil {
		return fmt.Errorf("create a new request: %w", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("x-budibase-app-id", appID)
	req.Header.Set("x-budibase-api-key", apiKey)

	resp, err := h.httpCLient.Do(req)
	if err != nil {
		return fmt.Errorf("doing request: %w", err)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("read response body: %w", err)
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("status code: %d %s", resp.StatusCode, string(body))
	}

	return nil
}

// UpdateApplication updates the permit application in budibase.
func (h *Handler) UpdateApplication(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	ctx := r.Context()

	var md domibus.MessageDispatch
	if err := json.NewDecoder(r.Body).Decode(&md); err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		h.logger.Error("update-application", "decode-message-dispatch", err)
		return
	}

	var perm permit.Permit
	err := json.Unmarshal(md.Payload, &perm)
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		h.logger.Error("update-application", "unmarshal-permit", err)
		return
	}

	// Set the recipient.
	recipient, ok := h.catalogIDs[perm.Application.CatalogID]
	if !ok {
		web.Respond(r.Context(), w,
			web.NewErrorResponse(fmt.Sprintf("no node for this catalog ID: %q", perm.Application.CatalogID)),
			http.StatusBadRequest)
		return
	}

	h.party.Recipient = recipient

	rowID, err := h.searchRow(ctx, perm.Application.ID)
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		h.logger.Error("update-application", "search-row", err)
		if err := trackmsg.ReplyTrackMsg(r.Context(), h.domibusSOAP, "data-permit",
			md.MessageID, http.StatusBadRequest, h.party); err != nil {
			h.logger.Error("update-application", "search-row", err)
		}
		return
	}

	if err := h.updateRow(ctx, rowID, perm.Application.Status); err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		h.logger.Error("update-application", "update-row", err)
		if err := trackmsg.ReplyTrackMsg(r.Context(), h.domibusSOAP, "data-permit",
			md.MessageID, http.StatusBadRequest, h.party); err != nil {
			h.logger.Error("update-application", "update-row", err)
		}
		return
	}

	w.WriteHeader(http.StatusOK)

	if err := trackmsg.ReplyTrackMsg(r.Context(), h.domibusSOAP, "data-permit",
		md.MessageID, http.StatusOK, h.party); err != nil {
		h.logger.Error("update-application", "update-success", err)
	}
}
