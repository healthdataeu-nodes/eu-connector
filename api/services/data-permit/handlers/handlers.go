// Package handlers manages the different versions of the API.
package handlers

import (
	"log/slog"
	"net/http"

	v1 "code.europa.eu/healthdataeu-nodes/eu-connector/api/services/data-permit/handlers/v1"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/edelivery/domibus"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/web/mid"

	"github.com/google/uuid"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

// APIMuxConfig contains all the mandatory systems required by handlers.
type APIMuxConfig struct {
	Logger      *slog.Logger
	DomibusSOAP *domibus.SOAP
	Party       domibus.Party
	CatalogIDs  map[uuid.UUID]string
}

// NewAPIMuxConfig constructs a new mux config.
func NewAPIMuxConfig(logger *slog.Logger, domibusSOAP *domibus.SOAP, party domibus.Party,
	catalogIDs map[uuid.UUID]string) APIMuxConfig {
	return APIMuxConfig{
		Logger:      logger,
		DomibusSOAP: domibusSOAP,
		Party:       party,
		CatalogIDs:  catalogIDs,
	}
}

// APIMux constructs a http.Handler with all application routes defined.
func APIMux(cfg APIMuxConfig) *mux.Router {
	router := mux.NewRouter()

	log := mid.Log{
		Logger: cfg.Logger,
	}

	corsMethod := []string{
		http.MethodPost,
		http.MethodGet,
		http.MethodOptions,
		http.MethodPut,
		http.MethodDelete,
	}

	corsHeaders := []string{
		"Accept",
		"Content-Type",
		"Content-Length",
		"Accept-Encoding",
		"X-CSRF-Token",
		"Authorization"}

	router.Use(log.Middleware, handlers.CORS(
		handlers.AllowedOrigins([]string{"*"}),
		handlers.AllowedMethods(corsMethod),
		handlers.AllowedHeaders(corsHeaders),
		handlers.MaxAge(86400),
		handlers.OptionStatusCode(http.StatusOK),
	))

	v1.Routes(router, v1.Config{
		Logger:      cfg.Logger,
		DomibusSOAP: cfg.DomibusSOAP,
		Party:       cfg.Party,
		CatalogIDs:  cfg.CatalogIDs,
	})

	return router
}
