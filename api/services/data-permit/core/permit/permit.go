// Package permit provides a core business API of a user.
package permit

import (
	"encoding/json"
	"fmt"

	"github.com/google/uuid"
)

// An Application contains the data needed to apply for a permit.
type Application struct {
	ID        int       `json:"application_id"`
	Status    string    `json:"status"`
	CatalogID uuid.UUID `json:"catalog_id"`
	Form      any       `json:"form"`
}

// A Permit is the payload of a message permit.
type Permit struct {
	Application Application `json:"application"`
}

// New is a factory function to create a new permit.
func New(applicationBody []byte) (Permit, error) {
	var application Application
	if err := json.Unmarshal(applicationBody, &application); err != nil {
		return Permit{}, fmt.Errorf("unmarshaling application payload: %w", err)
	}

	return Permit{
		Application: application,
	}, nil
}
