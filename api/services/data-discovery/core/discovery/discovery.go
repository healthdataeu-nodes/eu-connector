// Package discovery provides a core business API of a discovery.
package discovery

// Metadata is the information of a dataset and can be in different format.
type Metadata struct {
	Serialization string
	EncodedData   string
}

// Discovery contains dataset.
type Discovery struct {
	Dataset string
}
