package main

import (
	"context"
	"fmt"
	"log/slog"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"syscall"
	"time"

	"code.europa.eu/healthdataeu-nodes/eu-connector/api/services/data-discovery/handlers"
	"code.europa.eu/healthdataeu-nodes/eu-connector/api/services/data-discovery/handlers/v1/euportal"
	"code.europa.eu/healthdataeu-nodes/eu-connector/foundation/accesspoint"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/edelivery/domibus"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/externalapi"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/logger"

	"gopkg.in/yaml.v3"
)

func main() {
	ctx := context.Background()

	logger := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{AddSource: true, ReplaceAttr: logger.CustomAttr})).
		With("EU-CONNECTOR", "DATA-DISCOVERY")

	if err := run(ctx, logger); err != nil {
		logger.ErrorContext(ctx, "startup", "ERROR", err)
		os.Exit(1)
	}
}

func run(ctx context.Context, logger *slog.Logger) error {

	// ===================================================================================
	// Configuration

	confPath := "config/conf.yaml"

	cfg := struct {
		Web struct {
			ReadTimeout     time.Duration `yaml:"read_timeout"`
			WriteTimeout    time.Duration `yaml:"write_timeout"`
			IdleTimeout     time.Duration `yaml:"idle_timeout"`
			ShutdownTimeout time.Duration `yaml:"shutdown_timeout"`
			APIHost         string        `yaml:"api_host"`
		} `yaml:"data_discovery"`
		Domibus struct {
			Username   string `yaml:"username"`
			Host       string `yaml:"host"`
			DisableTLS bool   `yaml:"disable_tls"`
			Sender     string `yaml:"sender"`
		} `yaml:"domibus"`
		EUPortal struct {
			Username   string        `yaml:"username"`
			Host       string        `yaml:"host"`
			Timeout    time.Duration `yaml:"timeout"`
			DisableTLS bool          `yaml:"disable_tls"`
		} `yaml:"eu_portal"`
	}{}

	// ===================================================================================
	// App Starting

	logger.InfoContext(ctx, "starting service")
	defer logger.InfoContext(ctx, "shutdown complete")

	yamlFile, err := os.ReadFile(confPath)
	if err != nil {
		return fmt.Errorf("reading yaml file on %q: %w", confPath, err)
	}

	if err := yaml.Unmarshal(yamlFile, &cfg); err != nil {
		return fmt.Errorf("unmarshaling conf %q: %w", confPath, err)
	}

	logger.InfoContext(ctx, "startup", "config", cfg)

	// ===================================================================================
	// Domibus SOAP client

	logger.InfoContext(ctx, "startup", "status", "initializing Domibus SOAP client")

	domiScheme := "https"
	if cfg.Domibus.DisableTLS {
		domiScheme = "http"
	}

	domibusURL := &url.URL{
		Scheme: domiScheme,
		User:   url.User(cfg.Domibus.Username),
		Host:   cfg.Domibus.Host,
	}

	domibusSOAP, err := domibus.NewSOAP(domibusURL, http.DefaultClient)
	if err != nil {
		return fmt.Errorf("creating domibus soap client: %w", err)
	}

	// ===================================================================================
	// AP for CatalogID mapping

	logger.InfoContext(ctx, "startup", "status", "initializing Access Point for catalog ID mapping")

	aps, err := accesspoint.Mapping()
	if err != nil {
		return fmt.Errorf("mapping AP for catalog ID: %w", err)
	}

	// ===================================================================================
	// EU Portal config

	logger.InfoContext(ctx, "startup", "status", "initializing EU Portal config")

	euPortalConfig := euportal.Config{
		Username:   cfg.EUPortal.Username,
		Host:       cfg.EUPortal.Host,
		DisableTLS: cfg.EUPortal.DisableTLS,
		HTTPClient: externalapi.NewHTTPClient(logger),
		Timeout:    cfg.EUPortal.Timeout,
	}

	// ===================================================================================
	// Start API Service

	logger.InfoContext(ctx, "startup", "status", "initializing V1 API support")

	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, syscall.SIGINT, syscall.SIGTERM)

	muxConfig := handlers.NewAPIMuxConfig(logger, domibusSOAP, euPortalConfig, aps)
	apiMux := handlers.APIMux(muxConfig)

	serverErrors := make(chan error, 1)

	_, port, err := net.SplitHostPort(cfg.Web.APIHost)
	if err != nil {
		return fmt.Errorf("parsing port from %s: %w", cfg.Web.APIHost, err)
	}

	api := http.Server{
		Addr:         ":" + port,
		Handler:      apiMux,
		ReadTimeout:  cfg.Web.ReadTimeout,
		WriteTimeout: cfg.Web.WriteTimeout,
		IdleTimeout:  cfg.Web.IdleTimeout,
		ErrorLog:     slog.NewLogLogger(logger.Handler(), slog.LevelError),
	}

	go func() {
		logger.InfoContext(ctx, "startup", "status", "api router started", "host", api.Addr)
		serverErrors <- api.ListenAndServe()
	}()

	// ===================================================================================
	// Shutdown

	select {
	case err := <-serverErrors:
		return fmt.Errorf("server error: %w", err)

	case sig := <-shutdown:
		logger.InfoContext(ctx, "shutdown", "status", "shutdown started", "signal", sig)
		defer logger.InfoContext(ctx, "shutdown", "status", "shutdown complete", "signal", sig)

		ctx, cancel := context.WithTimeout(ctx, cfg.Web.ShutdownTimeout)
		defer cancel()

		if err := api.Shutdown(ctx); err != nil {
			api.Close()
			return fmt.Errorf("could not stop server gracefully: %w", err)
		}
	}

	return nil
}
