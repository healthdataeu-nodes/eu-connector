// Package handlers manages the different versions of the API.
package handlers

import (
	"log/slog"

	v1 "code.europa.eu/healthdataeu-nodes/eu-connector/api/services/data-discovery/handlers/v1"
	"code.europa.eu/healthdataeu-nodes/eu-connector/api/services/data-discovery/handlers/v1/euportal"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/edelivery/domibus"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/web/mid"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
)

// APIMuxConfig contains all the mandatory systems required by handlers.
type APIMuxConfig struct {
	Logger         *slog.Logger
	DomibusSOAP    *domibus.SOAP
	EUPortalConfig euportal.Config
	AccessPoints   map[string]uuid.UUID
}

// NewAPIMuxConfig constructs a new mux config.
func NewAPIMuxConfig(logger *slog.Logger, domibusSOAP *domibus.SOAP, euPortalConfig euportal.Config,
	aps map[string]uuid.UUID) APIMuxConfig {
	return APIMuxConfig{
		Logger:         logger,
		DomibusSOAP:    domibusSOAP,
		EUPortalConfig: euPortalConfig,
		AccessPoints:   aps,
	}
}

// APIMux constructs a http.Handler with all application routes defined.
func APIMux(cfg APIMuxConfig) *mux.Router {
	router := mux.NewRouter()

	log := mid.Log{
		Logger: cfg.Logger,
	}

	router.Use(log.Middleware, mux.CORSMethodMiddleware(router))

	v1.Routes(router, v1.Config{
		Logger:         cfg.Logger,
		DomibusSOAP:    cfg.DomibusSOAP,
		EUPortalConfig: cfg.EUPortalConfig,
		AccessPoints:   cfg.AccessPoints,
	})

	return router
}
