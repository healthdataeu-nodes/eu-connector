package euportal

// DatasetResponse represents the central platform identifier response.
type DatasetResponse struct {
	ID string `json:"dataset_id"`
}
