// Package euportal provides CRUD functionality to interact with EU Portal API.
package euportal

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"net/http"
	"net/url"
	"time"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/auth"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/externalapi"

	"github.com/google/uuid"
)

type Config struct {
	Username   string
	Host       string
	DisableTLS bool
	HTTPClient *http.Client
	Timeout    time.Duration
}

// fetchIdentifier fetchs the identifier of a dataset.
func fetchIdentfier(ctx context.Context, cfg Config, dataset string) (string, error) {
	ctx, cancel := context.WithTimeout(ctx, cfg.Timeout)
	defer cancel()

	hostname, port, err := net.SplitHostPort(cfg.Host)
	if err != nil {
		return "", fmt.Errorf("spliting host port: %w", err)
	}

	pass, err := auth.PassFile(cfg.Username, hostname, port, ".euportalpass")
	if err != nil {
		return "", fmt.Errorf("authenticate on EU Portal: %w", err)
	}

	header := make(http.Header)
	header.Set("X-API-KEY", pass)
	header.Set("Accept", "application/rdf+xml")
	header.Set("Content-type", "application/rdf+xml")

	path, err := url.JoinPath("identifiers", "datasets")
	if err != nil {
		return "", fmt.Errorf("join URL path: %w", err)
	}

	scheme := "https"
	if cfg.DisableTLS {
		scheme = "http"
	}

	euPortalURL := &url.URL{
		Scheme: scheme,
		User:   url.User(cfg.Username),
		Host:   cfg.Host,
		Path:   path,
	}

	euPortalAPI := externalapi.New(euPortalURL, cfg.HTTPClient, cfg.Timeout)
	resp, err := euPortalAPI.Request(ctx, http.MethodPost, []byte(dataset), header)
	if err != nil {
		return "", fmt.Errorf("post requet on eu portal: %w", err)
	}

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("status code: %s", resp.Status)
	}

	defer resp.Body.Close()

	identifierBinary, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("read response body: %w", err)
	}

	var datasetResp DatasetResponse
	if err := json.Unmarshal(identifierBinary, &datasetResp); err != nil {
		return "", fmt.Errorf("unmarshal json: %w", err)
	}

	return datasetResp.ID, nil
}

// Create creates a dataset.
func Create(ctx context.Context, cfg Config, dataset string, catID uuid.UUID) error {
	ctx, cancel := context.WithTimeout(ctx, cfg.Timeout)
	defer cancel()

	hostname, port, err := net.SplitHostPort(cfg.Host)
	if err != nil {
		return fmt.Errorf("spliting host port: %w", err)
	}

	pass, err := auth.PassFile(cfg.Username, hostname, port, ".euportalpass")
	if err != nil {
		return fmt.Errorf("authenticate on EU Portal: %w", err)
	}

	header := make(http.Header)
	header.Set("X-API-KEY", pass)
	header.Set("Accept", "application/rdf+xml")
	header.Set("Content-type", "application/rdf+xml")

	path, err := url.JoinPath("catalogues", catID.String(), "datasets")
	if err != nil {
		return fmt.Errorf("join URL path: %w", err)
	}

	scheme := "https"
	if cfg.DisableTLS {
		scheme = "http"
	}

	euPortalURL := &url.URL{
		Scheme: scheme,
		User:   url.User(cfg.Username),
		Host:   cfg.Host,
		Path:   path,
	}

	euPortalAPI := externalapi.New(euPortalURL, cfg.HTTPClient, cfg.Timeout)
	resp, err := euPortalAPI.Request(ctx, http.MethodPost, []byte(dataset), header)
	if err != nil {
		return fmt.Errorf("post request on eu portal: %w", err)
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("status code: %s", resp.Status)
	}

	return nil
}

// Update updates a dataset.
func Update(ctx context.Context, cfg Config, dataset string, catID uuid.UUID) error {
	ctx, cancel := context.WithTimeout(ctx, cfg.Timeout)
	defer cancel()

	identifier, err := fetchIdentfier(ctx, cfg, dataset)
	if err != nil {
		return fmt.Errorf("fetch identifier: %w", err)
	}

	hostname, port, err := net.SplitHostPort(cfg.Host)
	if err != nil {
		return fmt.Errorf("spliting host port: %w", err)
	}

	pass, err := auth.PassFile(cfg.Username, hostname, port, ".euportalpass")
	if err != nil {
		return fmt.Errorf("authenticate on eu portal: %w", err)
	}

	header := make(http.Header)
	header.Set("X-API-KEY", pass)
	header.Set("Accept", "application/rdf+xml")
	header.Set("Content-type", "application/rdf+xml")

	path, err := url.JoinPath("catalogues", catID.String(), "datasets", "origin")
	if err != nil {
		return fmt.Errorf("join URL path: %w", err)
	}

	scheme := "https"
	if cfg.DisableTLS {
		scheme = "http"
	}

	q := make(url.Values)
	q.Set("originalId", identifier)

	euPortalURL := &url.URL{
		Scheme:   scheme,
		User:     url.User(cfg.Username),
		Host:     cfg.Host,
		Path:     path,
		RawQuery: q.Encode(),
	}

	euPortalAPI := externalapi.New(euPortalURL, cfg.HTTPClient, cfg.Timeout)
	resp, err := euPortalAPI.Request(ctx, http.MethodPut, []byte(dataset), header)
	if err != nil {
		return fmt.Errorf("put request on eu portal: %w", err)
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("status code: %s", resp.Status)
	}

	return nil
}

// Delete deletes a dataset.
func Delete(ctx context.Context, cfg Config, dataset string) error {
	ctx, cancel := context.WithTimeout(ctx, cfg.Timeout)
	defer cancel()

	identifier, err := fetchIdentfier(ctx, cfg, dataset)
	if err != nil {
		return fmt.Errorf("fetch identifier: %w", err)
	}

	hostname, port, err := net.SplitHostPort(cfg.Host)
	if err != nil {
		return fmt.Errorf("spliting host port: %w", err)
	}

	pass, err := auth.PassFile(cfg.Username, hostname, port, ".euportalpass")
	if err != nil {
		return fmt.Errorf("authenticate on eu portal: %w", err)
	}

	header := make(http.Header)
	header.Set("X-API-KEY", pass)
	header.Set("Accept", "application/rdf+xml")
	header.Set("Content-type", "application/rdf+xml")

	path, err := url.JoinPath("datasets", identifier)
	if err != nil {
		return fmt.Errorf("join URL path: %w", err)
	}

	scheme := "https"
	if cfg.DisableTLS {
		scheme = "http"
	}

	euPortalURL := &url.URL{
		Scheme: scheme,
		User:   url.User(cfg.Username),
		Host:   cfg.Host,
		Path:   path,
	}

	euPortalAPI := externalapi.New(euPortalURL, cfg.HTTPClient, cfg.Timeout)
	resp, err := euPortalAPI.Request(ctx, http.MethodDelete, []byte(dataset), header)
	if err != nil {
		return fmt.Errorf("delete request on eu portal: %w", err)
	}

	if resp.StatusCode != http.StatusNoContent {
		return fmt.Errorf("status code: %s", resp.Status)
	}

	return nil
}
