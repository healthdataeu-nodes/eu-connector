// Package discoverygrp maintains the group of handlers for discovery access.
package discoverygrp

import (
	"encoding/json"
	"fmt"
	"log/slog"
	"net/http"

	"code.europa.eu/healthdataeu-nodes/eu-connector/api/services/data-discovery/core/discovery"
	"code.europa.eu/healthdataeu-nodes/eu-connector/api/services/data-discovery/handlers/v1/euportal"
	"code.europa.eu/healthdataeu-nodes/eu-connector/foundation/trackmsg"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/edelivery/domibus"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/web"

	"github.com/google/uuid"
)

type Handler struct {
	logger         *slog.Logger
	domibusSOAP    *domibus.SOAP
	euPortalConfig euportal.Config
	accessPoints   map[string]uuid.UUID
}

// New constructs a handlers for a route access.
func New(logger *slog.Logger, domibusSOAP *domibus.SOAP, euPortal euportal.Config,
	aps map[string]uuid.UUID) *Handler {
	return &Handler{
		logger:         logger,
		domibusSOAP:    domibusSOAP,
		euPortalConfig: euPortal,
		accessPoints:   aps,
	}
}

// Create creates a discovery dataset.
func (h *Handler) Create(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	var md domibus.MessageDispatch
	if err := json.NewDecoder(r.Body).Decode(&md); err != nil {
		h.logger.Error("create-discovery", "decode-message-dispatch", err)
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		return
	}

	replyParty := domibus.NewParty(md.Party.Recipient, md.Party.Sender)

	var disco discovery.Discovery
	err := json.Unmarshal(md.Payload, &disco)
	if err != nil {
		h.logger.Error("create-discovery", "unmarshal-discovery", err)
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		if err := trackmsg.ReplyTrackMsg(r.Context(), h.domibusSOAP, "data-discovery",
			md.MessageID, http.StatusBadRequest, replyParty); err != nil {
			h.logger.Error("create-discovery", "unmarshal-discovery", err)
		}
		return
	}

	catID, ok := h.accessPoints[md.Party.Sender]
	if !ok {
		web.Respond(r.Context(), w, web.NewErrorResponse("no catalogID related to the node sender"), http.StatusInternalServerError)
		h.logger.Error("create-discovery", "catalogID", fmt.Errorf("no cataog ID for this node: %q", md.Party.Sender))
		if err := trackmsg.ReplyTrackMsg(r.Context(), h.domibusSOAP, "data-discovery",
			md.MessageID, http.StatusInternalServerError, replyParty); err != nil {
			h.logger.Error("create-discovery", "catalogID", err)
		}
		return
	}

	if err := euportal.Create(r.Context(), h.euPortalConfig, disco.Dataset, catID); err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		if err := trackmsg.ReplyTrackMsg(r.Context(), h.domibusSOAP, "data-discovery",
			md.MessageID, http.StatusInternalServerError, replyParty); err != nil {
			h.logger.Error("create-discovery", "eu-portal", err)
		}
		return
	}

	w.WriteHeader(http.StatusCreated)

	if err := trackmsg.ReplyTrackMsg(r.Context(), h.domibusSOAP, "data-discovery",
		md.MessageID, http.StatusCreated, replyParty); err != nil {
		h.logger.Error("create-discovery", "create-success", err)
	}
}

// Update updates a discovery dataset.
func (h *Handler) Update(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	var md domibus.MessageDispatch
	if err := json.NewDecoder(r.Body).Decode(&md); err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		h.logger.Error("update-discovery", "decode-message-dispatch", err)
		return
	}

	replyParty := domibus.NewParty(md.Party.Recipient, md.Party.Sender)

	var disco discovery.Discovery
	err := json.Unmarshal(md.Payload, &disco)
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		h.logger.Error("update-discovery", "unmarshal-discovery", err)
		if err := trackmsg.ReplyTrackMsg(r.Context(), h.domibusSOAP, "data-discovery",
			md.MessageID, http.StatusCreated, replyParty); err != nil {
			h.logger.Error("update-discovery", "unmarshal-discovery", err)
		}
		return
	}

	catID, ok := h.accessPoints[md.Party.Sender]
	if !ok {
		web.Respond(r.Context(), w, web.NewErrorResponse("no catalogID related to the node sender"), http.StatusInternalServerError)
		h.logger.Error("update-discovery", "catalogID", fmt.Errorf("no cataog ID for this node: %q", md.Party.Sender))
		if err := trackmsg.ReplyTrackMsg(r.Context(), h.domibusSOAP, "data-discovery",
			md.MessageID, http.StatusInternalServerError, replyParty); err != nil {
			h.logger.Error("update-discovery", "catalogID", err)
		}
		return
	}

	if err := euportal.Update(r.Context(), h.euPortalConfig, disco.Dataset, catID); err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		if err := trackmsg.ReplyTrackMsg(r.Context(), h.domibusSOAP, "data-discovery",
			md.MessageID, http.StatusInternalServerError, replyParty); err != nil {
			h.logger.Error("update-discovery", "eu-portal", err)
		}
		return
	}

	w.WriteHeader(http.StatusNoContent)
	if err := trackmsg.ReplyTrackMsg(r.Context(), h.domibusSOAP, "data-discovery",
		md.MessageID, http.StatusNoContent, replyParty); err != nil {
		h.logger.Error("update-discovery", "update-success", err)
	}
}

// Delete deletes a discovery dataset.
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	var md domibus.MessageDispatch
	if err := json.NewDecoder(r.Body).Decode(&md); err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		h.logger.Error("delete-discovery", "decode-message-dispatch", err)
		return
	}

	replyParty := domibus.NewParty(md.Party.Recipient, md.Party.Sender)

	var disco discovery.Discovery
	err := json.Unmarshal(md.Payload, &disco)
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		h.logger.Error("delete-discovery", "unmarshal-discovery", err)
		if err := trackmsg.ReplyTrackMsg(r.Context(), h.domibusSOAP, "data-discovery",
			md.MessageID, http.StatusBadRequest, replyParty); err != nil {
			h.logger.Error("delete-discovery", "unmarshal-discovery", err)
		}
		return
	}

	_, ok := h.accessPoints[md.Party.Sender]
	if !ok {
		web.Respond(r.Context(), w, web.NewErrorResponse("no catalogID related to the node sender"), http.StatusInternalServerError)
		h.logger.Error("delete-discovery", "catalogID", fmt.Errorf("no cataog ID for this node: %q", md.Party.Sender))
		if err := trackmsg.ReplyTrackMsg(r.Context(), h.domibusSOAP, "data-discovery",
			md.MessageID, http.StatusInternalServerError, replyParty); err != nil {
			h.logger.Error("delete-discovery", "catalogID", err)
		}
		return
	}

	if err := euportal.Delete(r.Context(), h.euPortalConfig, disco.Dataset); err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		if err := trackmsg.ReplyTrackMsg(r.Context(), h.domibusSOAP, "data-discovery",
			md.MessageID, http.StatusInternalServerError, replyParty); err != nil {
			h.logger.Error("delete-discovery", "eu-portal", err)
		}
		return
	}

	w.WriteHeader(http.StatusNoContent)
	if err := trackmsg.ReplyTrackMsg(r.Context(), h.domibusSOAP, "data-discovery",
		md.MessageID, http.StatusNoContent, replyParty); err != nil {
		h.logger.Error("delete-discovery", "delete-success", err)
	}
}

// Restore restore a discovery catalog.
func (h *Handler) Restore(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	var md domibus.MessageDispatch
	if err := json.NewDecoder(r.Body).Decode(&md); err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		h.logger.Error("restore-discovery", "decode-message-dispatch", err)
		return
	}

	var disco discovery.Discovery
	err := json.Unmarshal(md.Payload, &disco)
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		h.logger.Error("restore-discovery", "unmarshal-discovery", err)
		return
	}

	_, ok := h.accessPoints[md.Party.Sender]
	if !ok {
		web.Respond(r.Context(), w, web.NewErrorResponse("no catalogID related to the node sender"), http.StatusInternalServerError)
		h.logger.Error("retore-discovery", "catalogID", fmt.Errorf("no cataog ID for this node: %q", md.Party.Sender))
		return
	}

	w.WriteHeader(http.StatusOK)
}
