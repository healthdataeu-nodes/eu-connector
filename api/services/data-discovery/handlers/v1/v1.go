// Package v1 contains the full set of handler functions and routes
// supported by the v1 web api.
package v1

import (
	"log/slog"
	"net/http"

	"code.europa.eu/healthdataeu-nodes/eu-connector/api/services/data-discovery/handlers/v1/discoverygrp"
	"code.europa.eu/healthdataeu-nodes/eu-connector/api/services/data-discovery/handlers/v1/euportal"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/edelivery/domibus"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
)

// Config contains all the mandatory systems required by handlers.
type Config struct {
	Logger         *slog.Logger
	DomibusSOAP    *domibus.SOAP
	EUPortalConfig euportal.Config
	AccessPoints   map[string]uuid.UUID
}

// Routes binds all the version 1 routes.
func Routes(router *mux.Router, cfg Config) {

	// Message dispatcher routes.
	dgh := discoverygrp.New(cfg.Logger, cfg.DomibusSOAP, cfg.EUPortalConfig, cfg.AccessPoints)
	router.HandleFunc("/create/dataset", dgh.Create).Methods(http.MethodPost)
	router.HandleFunc("/update/dataset", dgh.Update).Methods(http.MethodPost)
	router.HandleFunc("/delete/dataset", dgh.Delete).Methods(http.MethodPost)
	router.HandleFunc("/restore/catalog", dgh.Restore).Methods(http.MethodPost)
}
