// Package trackmsg provides a core business API to reply to a tracking message.
package trackmsg

import (
	"context"
	"encoding/json"
	"fmt"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/edelivery/domibus"
)

type ResponseTrackMsg struct {
	ID         string `json:"message_id"`
	StatusCode int    `json:"status_code"`
}

// createContent creates a track message content.
func createContent(dataType string, statusCode int, replyMessageID string) (string, error) {
	mtr := ResponseTrackMsg{
		ID:         replyMessageID,
		StatusCode: statusCode,
	}

	rawTm, err := json.Marshal(mtr)
	if err != nil {
		return "", fmt.Errorf("marshaling track message: %w", err)
	}

	msg, err := domibus.NewMessage(dataType, "track-messages/update", rawTm)
	if err != nil {
		return "", fmt.Errorf("creating new track message: %w", err)
	}

	b64Msg, err := domibus.EncodeMessage(msg)
	if err != nil {
		return "", fmt.Errorf("encoding track message: %w", err)
	}

	return b64Msg, nil
}

func ReplyTrackMsg(ctx context.Context, domibusSOAP *domibus.SOAP, dataType, replyMessageID string,
	statusCode int, party domibus.Party) error {
	b64Payload, err := createContent(dataType, statusCode, replyMessageID)
	if err != nil {
		return fmt.Errorf("create message track content: %w", err)
	}

	if _, err := domibusSOAP.SubmitMsgRequest(ctx, b64Payload, replyMessageID, party); err != nil {
		return fmt.Errorf("submit message track: %w", err)
	}

	return nil
}
