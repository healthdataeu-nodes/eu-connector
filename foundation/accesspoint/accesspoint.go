// Package accesspoint is about trusted eDelivery Access Point who has the right
// to communicate with the european node.
package accesspoint

import (
	"encoding/json"
	"fmt"
	"os"
	"path"

	"github.com/google/uuid"
)

// Mapping reads ap_catalog.json and put the key value pair in a map.
func Mapping() (map[string]uuid.UUID, error) {
	root, err := os.Getwd()
	if err != nil {
		return nil, fmt.Errorf("getting working directory: %w", err)
	}

	location := path.Join(root, "json", "ap_catalog.json")

	b, err := os.ReadFile(location)
	if err != nil {
		return nil, fmt.Errorf("reading %q: %w", location, err)
	}

	aps := make(map[string]uuid.UUID)
	err = json.Unmarshal(b, &aps)
	if err != nil {
		return nil, fmt.Errorf("unmarshaling %q: %w", location, err)
	}

	return aps, nil
}
