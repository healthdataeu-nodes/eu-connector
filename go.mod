module code.europa.eu/healthdataeu-nodes/eu-connector

go 1.22.4

require (
	code.europa.eu/healthdataeu-nodes/hdeupoc v0.0.0-20240628074702-57b3f381b1d2
	github.com/google/uuid v1.6.0
	github.com/gorilla/handlers v1.5.2
	github.com/gorilla/mux v1.8.1
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/JamMasterVilua/genericpass v1.0.3 // indirect
	github.com/felixge/httpsnoop v1.0.3 // indirect
	github.com/hooklift/gowsdl v0.5.0 // indirect
)

replace github.com/hooklift/gowsdl => code.europa.eu/healthdataeu-nodes/gowsdl v0.5.1-0.20231121105251-3e761f905e83
