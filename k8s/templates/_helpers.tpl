{{- define "db-hostname" -}}
{{- if .Values.db.enabled -}} 
{{ .Release.Name }}-postgres.{{ .Release.Namespace }}.svc.cluster.local
{{- else -}}
{{ required "externalHostname must be set when using own DB" .Values.db.connectorConfig.externalHostname }}
{{- end }}
{{- end }}

{{- define "db-port" -}}
{{- if .Values.db.enabled -}} 
5432
{{- else -}}
{{ .Values.db.connectorConfig.externalPort | default 5432 }}
{{- end }}
{{- end }}